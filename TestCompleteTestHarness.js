/*
This file contains objects and functions that can be used to test a given JScript file.  It has BDD capabilities.

example tests using BDD style:

describe("an example", function(){
it("Should compare true and true correctly", function(){
expect(true).to.equal(true);
});
});


alternatively you can do

var myTest = function(){
var five;
expect(five).to.equal(5);
}
describe("example test without anonymous function passed to the it function", function(){
it("should compare 5 and 5", myTest);
});


describe will print out the string you pass as a header, and every it function call will print the results under it.
e.g. the results for the above would look like

an example_____________________

PASS: Should compare true and true correctly


example test without anonymous function passed to the it function_________________

PASS: should compare 5 and 5

 */

/*

examples used in user documentation: 

describe("Example", function () {
    it("should do something an Example should do", function () {
        
        //define test data
        var five = 5;
        
        //make sure it does what we think
        expect(five).to.exist(); //passes
        expect(five).to.equal(5); //passes
    }); 
});

describe("AnotherExample", function () {
    it("should do something an AnotherExample should do", function () {
        
        //define test data
        var aBool = true;
        var isNotDefined;
        
        //make sure it does what we think
        expect(aBool).to.equal(true); //passes
        expect(aBool).to.equal(false); //fails
        expect(isNotDefined).to.exist(); //fails
    }); 
});
*/

var oShell = new ActiveXObject("WScript.Shell");
var oExec = oShell.Exec('cmd /c dir');
var fso = new ActiveXObject("Scripting.FileSystemObject");


//status constants
var PASS = "PASS";
var FAIL = "FAIL";
var WARN = "WARN";

//
//these variables and functions are used to keep track of test results while running tests.
//

var globalStatus;
var assertCount = 0;
var expectCount = 0;
var passCount = 0;
var failCount = 0;
var warnCount = 0;

function setStatus(status) {
    globalStatus = status;
}

function incrementAssertCount() {
    assertCount += 1;
}

function incrementExpectCount() {
    expectCount = expectCount + 1;
}

function incrementPassCount() {
    passCount += 1;
}

function incrementFailCount() {
    failCount += 1;
}

function incrementWarnCount() {
    warnCount += 1;
}

//
//Helper for script output
//
var Messenger = {
    message: function (msgHead, msgBody) {
        WScript.Echo(msgHead + ":\n" + msgBody);
    },
    messageNoHeader: function (msgBody) {
        WScript.Echo(msgBody + "\n");
    }
};

//
//todo:  let assert be called under it and keep track of status like the expect function
//
var assert = function (condition) {
    if (condition) {
        WScript.Echo("Passed:  " + name);
    } else if (!condition) {
        WScript.Echo("Failed:  " + name);
    } else {
        Messenger.message("INVALID ASSERTION", "Value passed to assert wasn't a conditional or boolean.  Value passed was:  " + condition);
    }

}

//
//the expect and it functions allow BDD style testing. it returns an object with the to function as a property
//

var expect = function (input) {

    var exists = true;

    //increment the expect count
    incrementExpectCount();

    //
    //determine if the input exists for the it.exists property
    //
    if (input && input != null && input != undefined) {
        exists = true;
    } else if (!input) {
        exists = false;
    }

    //
    //this function compares two arrays. fails if they are not the same length. doesn't work on any arrays above 2D
    //
    var compareArrays = function (array1, array2) {
        if (array1.length != array2.length) {
            return false;
        } else {
            for (var index = 0; index < array1.length; index++) {
                if (array1[index] != array2[index]) {
                    return false;
                }
            }
            //if none of the elements were different, return true
            return true;
        }
    };

    //
    //builds a string that formats actual vs expected values in the event of an error or warning
    //
    var getActualVsExpectedString = function (actual, expected) {
        return (" Actual value:  " + actual + "  \n Expected value:  " + expected);
    }



    //
    // the to object contains the bulk of the BDD functionality.
    //
    var to = {
        input: input,
        equal: function (compareTarget) {
            //check if the object is an array. if so, compare object as array
            if (Object.prototype.toString.call(input) === '[object Array]') {
                var areSame = compareArrays(input, compareTarget);

                if (areSame) {
                    if (globalStatus != FAIL && globalStatus != WARN) {
                        setStatus(PASS);
                    }

                    return true;
                } else if (!areSame) {

                    setStatus(FAIL);
                    Log.Error("expect(...).to.equal failed. \n" + getActualVsExpectedString(to.input, compareTarget));
                    return false;
                }
            }

            //primitive or build in object comparison (e.g. strings, integers, etc.)
            if (to.input === compareTarget) {

                //only set to pass if status isn't set to fail or warn
                if (globalStatus != FAIL && globalStatus != WARN) {
                    setStatus(PASS);
                }
                //Log.Message("expect(...).to.equal passed");
                return true;
            } else if (to.input != compareTarget) {
                setStatus(FAIL);
                Log.Error("expect(...).to.equal failed." + getActualVsExpectedString(to.input, compareTarget));
                return false;
            } else {
                setStatus(WARN);
                Log.Error("Can't determine if arguments are equal");
            }

        },


        exist: function () {
            if (exists == true) {

                //if status isn't already set to fail or warn, set it to pass
                if (globalStatus != FAIL && globalStatus != WARN) {
                    setStatus(PASS);

                }
            } else {
                //if it doesn't exist, set the status to fail and increment the fail count
                setStatus(FAIL);
            }
            return exists;
        }
    }
    return {
        to: to
    };
}

var it = function (should, testFn) {

    //
    //set globals to default state
    //
    globalStatus = undefined;
    expectCount = 0;
    assertCount = 0;

    //execute the test function if it exists
    if (testFn) {
        testFn();
    }


    if (globalStatus === undefined) {
        globalStatus = "NO EXPECTATIONS"
    }


    if (globalStatus == PASS) {
        incrementPassCount();
    } else if (globalStatus == FAIL) {
        incrementFailCount();
    } else if (globalStatus == WARN) {
        incrementWarnCount();
    }
    Log.Message(globalStatus + ": " + should + "   [ " + expectCount + " expects/asserts ]");

};

var describe = function (description, testSuiteFn) {
    //
    //set globals to default state. their values will be changed with calls of expect(...) and it(...)
    //
    passCount = 0;
    failCount = 0;
    warnCount = 0;

    Log.Message("_______________________________________________________________________________");
    Log.Message("\n ************  " + description + "  ************");
    testSuiteFn();
    Log.Message("- - - - - Results - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
    Log.Message(passCount + " PASSED; " + failCount + " FAILED; " + warnCount + " WARNINGS");


    Log.Message(passCount + failCount + warnCount + " TEST ITEM(S)");
    /*    if (expectCount !== 0) {
            Log.Message(expectCount + " EXPECT STATEMENTS TOTAL");
        }
        if (assertCount !== 0) {
            Log.Message(assertCount + " ASSERT STATEMENTS TOTAL");
        }

        if (expectCount !== 0 && assertCount !== 0) {
            Log.Message("Beware! You are using both expect and assert style tests in the same script.");
        }*/
    Log.Message("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
};

//this function changes the log functionality for the sake of testing (mocks spirateam functions of the same name)
var Log = {
    //this function
    Message: function (msg) {
        Messenger.messageNoHeader(msg);
    },

    Error: function (msg) {
        Messenger.message("Log.Error", msg);
        //WScript.Quit(0);
    },

    Warning: function (msg) {
        Messenger.message("Log.Warning", msg);
    }
};